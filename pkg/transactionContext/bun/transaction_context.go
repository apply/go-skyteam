package bun

import (
	"context"
	"github.com/uptrace/bun"
)

type TransactionContext struct {
	Db      *bun.DB
	Tx      bun.Tx
	Context context.Context
}

func (transactionContext *TransactionContext) StartTransaction() error {
	tx, err := transactionContext.Db.Begin()
	if err != nil {
		return err
	}
	transactionContext.Tx = tx
	return nil
}

func (transactionContext *TransactionContext) CommitTransaction() error {
	err := transactionContext.Tx.Commit()
	return err
}

func (transactionContext *TransactionContext) RollbackTransaction() error {
	err := transactionContext.Tx.Rollback()
	return err
}

func NewTransactionContext(db *bun.DB) *TransactionContext {
	return &TransactionContext{
		Db:      db,
		Context: context.Background(),
	}
}
