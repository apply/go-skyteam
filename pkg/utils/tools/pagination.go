package tools

import "github.com/uptrace/bun"

func BunPagination(query *bun.SelectQuery, options map[string]interface{}) {
	if pageSize,ok := options["pageSize"];ok {
		pageNumber := int64(1)
		if _,ok := options["pageNumber"];ok {
			pageNumber = options["pageNumber"].(int64)
		}
		if pageSize.(int64) < 0 {
			pageSize = 10
		}
		offset := (pageNumber - 1) * pageSize.(int64)
		limit := pageSize.(int64)
		query.Offset(int(offset)).Limit(int(limit))
	}
}
