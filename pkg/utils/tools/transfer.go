package tools

import (
	"bytes"
	"encoding/gob"
	"gitee.com/apply/go-skyteam/pkg/utils/json"
	"reflect"
)

//拷贝对象
func DeepCopy(src, dst interface{}) error {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(src); err != nil {
		mBytes,err := json.Marshal(src)
		if err != nil {
			return err
		}
		err = json.Unmarshal(mBytes,&dst)
		return err
	}
	return gob.NewDecoder(bytes.NewBuffer(buf.Bytes())).Decode(dst)
}

//结构体指针转map
func StructPoint2Map(obj interface{}) map[string]interface{} {
	elem := reflect.ValueOf(obj).Elem()
	relType := elem.Type()
	m := make(map[string]interface{})
	for i := 0; i < relType.NumField(); i++ {
		m[LowerFirst(relType.Field(i).Name)] = elem.Field(i).Interface()
	}
	return m

	//mBytes, _ := json.Marshal(obj)
	//var result map[string]interface{}
	//_ = json.Unmarshal(mBytes, &result)
	//return result
}

//表格输出
func SimpleWrapGridMap(total int64, list interface{}) map[string]interface{} {
	grid := map[string]interface{}{"total": total, "list": list}
	return map[string]interface{}{
		"grid": grid,
	}
}
