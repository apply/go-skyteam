package filters

import (
	"gitee.com/apply/go-skyteam/pkg/core/log"
	"github.com/beego/beego/v2/server/web/context"
)

func CreateRequestLogFilter(logger log.Logger) func(ctx *context.Context) {
	return func(ctx *context.Context) {
		var append = make(map[string]interface{})
		append["framework"] = "beego"
		append["method"] = ctx.Input.Method()
		append["url"] = ctx.Input.URL()
		if ctx.Input.Is("GET") {
			logger.Debug("http请求", append)
		} else if ctx.Input.Is("POST") {
			append["inputData"] = string(ctx.Input.RequestBody)
			logger.Debug("http请求", append)
		} else {
			logger.Info("http请求", append)
		}
	}
}
