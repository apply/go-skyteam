module gitee.com/apply/go-skyteam

go 1.16

require (
	github.com/beego/beego/v2 v2.0.0 // indirect
	github.com/bwmarrin/snowflake v0.3.0 // indirect
	github.com/uptrace/bun v1.0.20 // indirect
)
